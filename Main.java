class Food {
  public String name;
    
  Food(String n) {
    name = n;
  }
}

class Human {
  public String foodIDontLike;
  public String name;

  Human(String f, String n) {
    foodIDontLike = f;
    name = n;
  }

  public void eat(Food food) {
    if (food.name == foodIDontLike) {
      System.out.println(name + " doesn't like " + food.name);
    } else {
      System.out.println(name + " is eating " + food.name);
    }
  }
}

class Main {
  public static void main(String args[]) {
    Human kirill = new Human("milk", "Kirill");
    Food milk = new Food("milk");
    Food meat = new Food("meat");

    kirill.eat(milk);
    kirill.eat(meat);
    System.out.println("Conflict!");
  }
}
